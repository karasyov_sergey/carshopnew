package ru.kso.carshop;

import android.content.Context;

import android.content.res.AssetManager;

import android.database.Cursor;

import android.database.sqlite.SQLiteDatabase;

import androidx.test.platform.app.InstrumentationRegistry;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;

import org.junit.Before;

import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.BufferedReader;

import java.io.IOError;

import java.io.IOException;

import java.io.InputStreamReader;

import java.util.ArrayList;

import ru.kso.carshop.db.DBHelper;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4.class)

public class ExampleInstrumentedTest {

    private Context context;

    private DBHelper dbHelper;

    private AssetManager manager;

    private SQLiteDatabase db;

    private Cursor cursor;

    private ArrayList<String> data;

    @Before

    public void prepareData() {

        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        manager = context.getAssets();
        dbHelper = new DBHelper(context);
        db = dbHelper.getWritableDatabase();
        cursor = db.query(DBHelper.TABLE_CARS, null, null,

                null, null, null, null);
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(manager.open("data.txt")));
            String line;
            data = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                data.add(line);
            }

        } catch (IOException ignored) {

        }

    }

    @Test
    public void notNullTest() {
        assertNotNull(data);
    }

    @Test
    public void dataTest() {
        if (data != null) {
            assertEquals(240, data.size());
        }
    }

    @Test
    public void useAppContext() {
        assertEquals("ru.kso.carshop", context.getPackageName());
    }

    @Test()
    public void dbTest() {
        int j = 0;
        int k = 0;
        String[] columns = cursor.getColumnNames();
        if (cursor.moveToFirst()) {
            int id = 1;
            do {
                for (int i = 0; i < cursor.getCount(); i++) {
                    if (k == 0) {
                        assertEquals(id, cursor.getInt(
                                cursor.getColumnIndex(columns[k])));
                    } else if (k == 15) {
                        assertEquals(DBHelper.ANDROID_PATH + data.get(j),
                                cursor.getString(cursor.getColumnIndex(columns[k])));
                        j++;
                    } else {
                        assertEquals(data.get(j), cursor.getString(
                                cursor.getColumnIndex(columns[k])));
                        j++;
                    }
                    k++;
                }
                k = 0;
                id++;
            } while (cursor.moveToNext());
        }
    }

    @After
    public void clearData() {
        context = null;
        dbHelper = null;
        db = null;
        cursor = null;
        data = null;
        manager = null;
    }
}