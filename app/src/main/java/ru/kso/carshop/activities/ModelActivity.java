package ru.kso.carshop.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import ru.kso.carshop.R;
import ru.kso.carshop.db.DBHelper;
import ru.kso.carshop.helper.IConstants;

/**
 * Класс, представляющий собой активность, которая
 * отображает подробные данные о выбранной модели
 * автомобиля
 *
 * @author KSO 17IT17
 */
public class ModelActivity extends AppCompatActivity implements
        View.OnClickListener, IConstants {

    private SQLiteDatabase db;
    private int arg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.model_activity);
        ImageView imageView = findViewById(R.id.image_model_big);
        TextView textView = findViewById(R.id.data);
        Button button = findViewById(R.id.btn_to_basket);
        button.setOnClickListener(this);
        Intent intent = getIntent();
        DBHelper dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();
        arg = intent.getIntExtra(KEY_ID, 0);
        String[] data = getApplication().getResources().getStringArray(R.array.data);
        Cursor cursor = db.rawQuery("select * from " + DBHelper.TABLE_CARS +
                " where " + DBHelper.KEY_MODEL_ID + " = " + arg, null);
        if (cursor.moveToFirst()) {
            String dataTextView;
            String temp;
            for (int i = 1; i < cursor.getColumnCount(); i++) {
                dataTextView = textView.getText().toString();
                temp = cursor.getString(cursor.
                        getColumnIndex(cursor.getColumnNames()[i]));
                if (i == 15) {
                    setImage(imageView, temp);
                } else {
                    updateText(textView, dataTextView, data[i - 1], temp);
                }
            }
        }
        cursor.close();

    }

    /**
     * Обновляет данные текстового поля
     *
     * @param textView     текстовое поле
     * @param dataTextView данные текстового поля
     * @param parameter    строка, относительно которой сформируется
     *                     добавляемый текст
     * @param data         добавляемые данные
     */
    private void updateText(TextView textView, String dataTextView,
                            String parameter, String data) {
        String temp = String.format(parameter, data);
        dataTextView = dataTextView.concat(temp);
        textView.setText(dataTextView);
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView
     *
     * @param imageView интерфейсный компонент, отображающий
     *                  изображение
     * @param path      путь к изображению
     */
    private void setImage(ImageView imageView, String path) {
        Glide.with(getApplicationContext()).asBitmap().
                load(path).
                into(imageView);
    }

    @Override
    public void onClick(View v) {
        String email = getSharedPreferences(KEY_PREFERENCES,
                Context.MODE_PRIVATE).getString(KEY_EMAIL, null);
        if (email == null) {
            Toast.makeText(this, getString(R.string.authorize), Toast.LENGTH_LONG).show();
            return;
        }
        Cursor cursor = db.rawQuery("select * from " + DBHelper.TABLE_BASKET,
                null);
        if (!isFoundSame(cursor, email, arg)) {

            ContentValues values = new ContentValues();

            values.put(DBHelper.KEY_MODEL_ID, arg);
            values.put(DBHelper.KEY_EMAIL, email);

            db.insertWithOnConflict(DBHelper.TABLE_BASKET,
                    null, values, SQLiteDatabase.CONFLICT_NONE);
            Toast.makeText(this, getString(R.string.added),
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, getString(R.string.have_got),
                    Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Метод, который проверяет на наличие текущей модели
     * автомобиля в корзине пользователя
     *
     * @param cursor курсор, отбирающий данные из таблицы по запросу
     * @param email  электронная почта пользователя
     * @param arg    аргумент, представляет собой id автомобиля
     * @return true если совпадение нашлось иначе false
     */
    private boolean isFoundSame(Cursor cursor, String email, int arg) {
        boolean bool = false;
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBHelper.KEY_MODEL_ID);
            int emailIndex = cursor.getColumnIndex(DBHelper.KEY_EMAIL);
            do {
                if (cursor.getInt(idIndex) == arg &&
                        cursor.getString(emailIndex).equals(email)) {
                    bool = true;
                    break;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return bool;
    }


}
