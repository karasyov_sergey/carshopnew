package ru.kso.carshop.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Класс, представляющий собой базу данных, содержащая
 * в себе некоторые таблицы
 *
 * @author KSO17IT17
 */
public class DBHelper extends SQLiteOpenHelper implements IDB {
    private Context context;
    private AssetManager manager;

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        manager = context.getAssets();

        db.execSQL("create table " + TABLE_CARS + "( "
                + KEY_MODEL_ID + " integer primary key autoincrement, "
                + KEY_BRAND + " text NOT NULL, "
                + KEY_MODEL + " text NOT NULL, "
                + KEY_EQUIPMENT + " text NOT NULL, "
                + KEY_PRICE + " integer NOT NULL, "
                + KEY_BODY_TYPE + " text NOT NULL, "
                + KEY_COUNTRY_OF_ASSEMBLY + " text NOT NULL, "
                + KEY_NUMBER_OF_PLACES + " integer NOT NULL, "
                + KEY_WEIGHT + " integer NOT NULL, "
                + KEY_MAX_CARRYING + " integer NOT NULL, "
                + KEY_V_FUEL + " integer NOT NULL, "
                + KEY_V_TRUNK + " integer NOT NULL, "
                + KEY_ENGINE_BRAND + " text NOT NULL, "
                + KEY_FUEL_TYPE + " text NOT NULL, "
                + KEY_MAX_POWER + " integer NOT NULL, "
                + KEY_MODEL_IMG + " text NOT NULL )"
        );
        db.execSQL("create table " + TABLE_ACCOUNTS + " ( "
                + KEY_NAME + " text NOT NULL, "
                + KEY_PASSWORD + " text NOT NULL, "
                + KEY_EMAIL + " text NOT NULL)"
        );
        db.execSQL("create table " + TABLE_BASKET + "( "
                + KEY_EMAIL + " text NOT NULL, "
                + KEY_MODEL_ID + " integer NOT NULL )"
        );
        initCarTable(db);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Инициализирует таблицу автомобилей
     *
     * @param db база данных
     */
    private void initCarTable(SQLiteDatabase db) {
        try {

            Cursor cursor = db.query(TABLE_CARS, null,
                    null, null, null,
                    null, null);
            ContentValues values = new ContentValues();
            ArrayList<String> data = getData();
            int dataIndex = 0;
            for (int i = 1; i <= data.size() / 15; i++) {
                for (int columnIndex = 1; columnIndex < cursor.getColumnCount();
                     columnIndex++) {
                    if (columnIndex == 4 || columnIndex == 7 ||
                            columnIndex == 8 || columnIndex == 9 ||
                            columnIndex == 10 || columnIndex == 11 ||
                            columnIndex == 14) {
                        values.put(cursor.getColumnNames()[columnIndex],
                                Integer.parseInt(data.get(dataIndex)));
                    } else if (columnIndex == 15) {
                        values.put(cursor.getColumnNames()[columnIndex],
                                ANDROID_PATH + data.get(dataIndex));
                    } else {
                        values.put(cursor.getColumnNames()[columnIndex],
                                data.get(dataIndex));
                    }
                    dataIndex++;
                }
                db.insert(TABLE_CARS, null, values);
                cursor.moveToNext();
            }
            cursor.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Читает и сохраняет текстовые данные для
     * таблицы автомобилей из текстового файла
     *
     * @return списочный массив, в каждом
     * элементе которого хранится 1 строка текстового файла
     * @throws IOException исключение, выбрасываемое при ошибке
     *                     чтения или записи на файл
     */
    private ArrayList<String> getData() throws IOException {
        ArrayList<String> temp = new ArrayList<>();
        BufferedReader bf = new BufferedReader(new InputStreamReader(
                manager.open("data.txt")));
        String line;
        while ((line = bf.readLine()) != null) {
            temp.add(line);
        }
        bf.close();
        return temp;
    }

}
