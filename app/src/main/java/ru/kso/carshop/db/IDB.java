package ru.kso.carshop.db;

/**
 * Интерфейс, который содержит название баз данных, таблиц и их ключей
 *
 * @author KSO 17IT17
 */
public interface IDB {
    String ANDROID_PATH = "file:///android_asset/";

    String DB_NAME = "cars";

    String TABLE_CARS = "car_table";
    String TABLE_ACCOUNTS = "accounts_table";
    String TABLE_BASKET = "basket_table";

    String KEY_BRAND = "brand";
    String KEY_MODEL = "model";
    String KEY_MODEL_IMG = "model_img";
    String KEY_NAME = "name";
    String KEY_PASSWORD = "password";
    String KEY_EMAIL = "email";
    String KEY_MODEL_ID = "model_id";
    String KEY_EQUIPMENT = "equipment";
    String KEY_PRICE = "price";
    String KEY_BODY_TYPE = "body_type";
    String KEY_COUNTRY_OF_ASSEMBLY = "country_of_assembly";
    String KEY_NUMBER_OF_PLACES = "number_of_places";
    String KEY_WEIGHT = "weight";
    String KEY_MAX_CARRYING = "max_carrying";
    String KEY_V_FUEL = "v_fuel";
    String KEY_V_TRUNK = "v_trunk";
    String KEY_ENGINE_BRAND = "engine_brand";
    String KEY_FUEL_TYPE = "fuel_type";
    String KEY_MAX_POWER = "max_power";
}
