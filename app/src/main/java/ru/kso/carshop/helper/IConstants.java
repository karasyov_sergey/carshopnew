package ru.kso.carshop.helper;

/**
 * Интерфейс, содержащий строкоые константы
 *
 * @author KSO 17IT17
 */
public interface IConstants {
    String KEY_EMAIL = "email";
    String KEY_NAME = "name";
    String KEY_PREFERENCES = "AccData";
    String NULL_VALUE = null;
    String KEY_ID = "id";

}
