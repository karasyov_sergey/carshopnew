package ru.kso.carshop.ui.authorization;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import ru.kso.carshop.R;
import ru.kso.carshop.activities.MainActivity;
import ru.kso.carshop.db.DBHelper;
import ru.kso.carshop.helper.IConstants;

/**
 * Класс, представляющий собой фрагмент, отображающий
 * необходимые интерфейсные компоненты для авторизации
 * зарегистрированного пользователя
 *
 * @author KSO 17IT17
 */
public class AuthorizationFragment extends Fragment implements
        View.OnClickListener, IConstants {

    private EditText inputEmail;
    private EditText inputPassword;
    private SQLiteDatabase db;
    private String userName;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_authorization,
                container, false);
        initialize(root);
        return root;
    }

    /**
     * Инициализирует интерфейсные компоненты для
     * текущего фрагмента
     *
     * @param root базовый интерфейсный блок
     */
    private void initialize(View root) {
        DBHelper mDBHelper = new DBHelper(root.getContext());
        db = mDBHelper.getWritableDatabase();
        inputEmail = root.findViewById(R.id.inputEmail);
        inputPassword = root.findViewById(R.id.inputPassword);
        Button button = root.findViewById(R.id.btnAuthorization);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();
        if (email.isEmpty()) {
            inputEmail.setError(getString(R.string.enter_email));
            return;
        }
        if (password.isEmpty()) {
            inputPassword.setError(getString(R.string.enter_password));
            return;
        }

        if (isSuccess(email, password)) {
            Toast.makeText(v.getContext(),
                    getString(R.string.successful_authorization),
                    Toast.LENGTH_LONG).show();
            inputEmail.setText(null);
            inputPassword.setText(null);
            SharedPreferences sp = v.getContext().
                    getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(KEY_NAME, userName);
            editor.putString(KEY_EMAIL, email);
            editor.apply();

            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) {
                activity.setHeaderName();
                activity.setFragment(R.id.nav_shop);
            }

        } else {
            inputEmail.setError(getString(R.string.invalid_email));
            inputPassword.setError(getString(R.string.invalid_password));
        }
    }

    /**
     * Проверяет, корректные ли данные ввел пользователь
     * своего аккаунта
     *
     * @param email эл. почта зарегистрированного пользователя
     * @param password пароль зарегистрированного пользователя
     *
     * @return true если пользователь ввел данные своего аккаунта верно иначе false
     */
    private boolean isSuccess(String email, String password) {
        boolean bool = false;
        Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS, null,
                null, null, null,
                null, null);
        if (cursor.moveToFirst()) {
            int emailIndex = cursor.getColumnIndex(DBHelper.KEY_EMAIL);
            int passwordIndex = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            do {
                if (email.equals(cursor.getString(emailIndex)) &&
                        password.equals(cursor.getString(passwordIndex))) {
                    userName = cursor.getString(nameIndex);
                    bool = true;
                    break;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return bool;
    }
}