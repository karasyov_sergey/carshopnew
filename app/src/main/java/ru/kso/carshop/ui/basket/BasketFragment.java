package ru.kso.carshop.ui.basket;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ru.kso.carshop.R;
import ru.kso.carshop.adapter.ShopAdapter;
import ru.kso.carshop.db.DBHelper;
import ru.kso.carshop.helper.IConstants;


/**
 * Класс, представляющий собой фрагмент, отображает
 * корзину на экране пользователя
 *
 * @author KSO 17IT17
 */
public class BasketFragment extends Fragment implements IConstants {
    private SQLiteDatabase db;
    private String query;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_basket, container, false);
        initBasket(root);

        return root;
    }

    /**
     * Инициализирует необходимые интерфейсные компоненты
     * корзины авторизованного пользователя
     *
     * @param root базовый интерфейсный блок
     */
    private void initBasket(View root) {
        DBHelper dbHelper = new DBHelper(root.getContext());
        db = dbHelper.getWritableDatabase();
        String email = root.getContext().getSharedPreferences(KEY_PREFERENCES,
                Context.MODE_PRIVATE).getString(KEY_EMAIL, NULL_VALUE);
        if (email != null) {
            initData(email);
            RecyclerView recyclerView = root.findViewById(R.id.basket);
            LinearLayoutManager manager = new LinearLayoutManager(root.getContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(manager);
            ShopAdapter shopAdapter = new ShopAdapter(root.getContext(), db, query, null);
            recyclerView.setAdapter(shopAdapter);
        }
    }

    /**
     * Инициализирует данные для последующего отображения
     * в корзине авторизованного пользователя
     *
     * @param email эл. почта авторизованного пользователя
     */
    private void initData(String email) {
        Cursor cursor = db.query(DBHelper.TABLE_BASKET,
                new String[]{DBHelper.KEY_MODEL_ID}, DBHelper.KEY_EMAIL + "= ?",
                new String[]{email}, null, null, null);
        int[] arrId = new int[cursor.getCount()];
        if (cursor.moveToFirst()) {
            int modelId = cursor.getColumnIndex(DBHelper.KEY_MODEL_ID);
            int i = 0;
            do {
                arrId[i] = cursor.getInt(modelId);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();

        String args = getArgs(arrId);
        query = "select * from " + DBHelper.TABLE_CARS +
                " where " + DBHelper.KEY_MODEL_ID + " = " + args;

    }

    /**
     * Формирует строку аргументов для
     * запроса на языке SQL для таблицы базы данных
     *
     * @param arrId массив целочисленных аргументов
     * @return строку аргументов
     */
    private String getArgs(int[] arrId) {
        if (arrId.length == 0) {
            return "0";
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arrId.length; i++) {
            if (i == arrId.length - 1) {
                builder.append(arrId[i]);
            } else {
                builder.append(arrId[i]).append(" or " + DBHelper.KEY_MODEL_ID + " = ");
            }
        }
        return builder.toString();
    }
}