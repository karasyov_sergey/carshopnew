package ru.kso.carshop.ui.registration;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import ru.kso.carshop.R;
import ru.kso.carshop.activities.MainActivity;
import ru.kso.carshop.db.DBHelper;

/**
 * Класс, представляющий собой фрагмент, отображающий
 * интерфейсные компоненты, учавствующие в создании
 * аккаунта
 *
 * @author KSO 17IT17
 */
public class RegistrationFragment extends Fragment implements View.OnClickListener {
    private SQLiteDatabase db;
    private EditText inputEmail;
    private EditText inputName;
    private EditText inputPassword;
    private EditText inputPasswordRepeat;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_registration,
                container, false);
        initialize(root);
        return root;
    }

    /**
     * Инициализирует необходимые интерфейсные компоненты
     *
     * @param root базовый интерфейсный блок
     */
    private void initialize(View root) {
        DBHelper dbHelper = new DBHelper(root.getContext());
        db = dbHelper.getWritableDatabase();
        inputEmail = root.findViewById(R.id.inputEmail);
        inputName = root.findViewById(R.id.inputName);
        inputPassword = root.findViewById(R.id.inputPassword);
        inputPasswordRepeat = root.findViewById(R.id.inputPasswordRepeat);
        Button button = root.findViewById(R.id.btnRegistration);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = inputEmail.getText().toString();
        String name = inputName.getText().toString().trim();
        String password = inputPassword.getText().toString();
        String passwordRepeat = inputPasswordRepeat.getText().toString();
        if (!isValidData(email, name, password, passwordRepeat)) {
            return;
        }
        if (isFreeAcc(email)) {
            ContentValues values = new ContentValues();
            values.put(DBHelper.KEY_EMAIL, email);
            values.put(DBHelper.KEY_NAME, name);
            values.put(DBHelper.KEY_PASSWORD, password);
            db.insert(DBHelper.TABLE_ACCOUNTS, null, values);
            inputEmail.setText(null);
            inputName.setText(null);
            inputPassword.setText(null);
            inputPasswordRepeat.setText(null);
            Toast.makeText(v.getContext(), getString(R.string.successful_reg),
                    Toast.LENGTH_LONG).show();
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) {
                activity.setFragment(R.id.nav_authorization);
            }
        } else {
            inputEmail.setError(getString(R.string.busy_email));
        }
    }

    /**
     * Метод, который проверяет базу данных на
     * наличие эл. почты, введенной пользователем
     *
     * @param email эл. почта, введенная пользователем
     * @return true если совпадений не нашлось иначе false
     */
    private boolean isFreeAcc(String email) {
        Cursor cursor = db.query(DBHelper.TABLE_ACCOUNTS,
                new String[]{DBHelper.KEY_EMAIL},
                null, null, null,
                null, null);
        if (cursor.moveToFirst()) {
            int emailIndex = cursor.getColumnIndex(DBHelper.KEY_EMAIL);
            do {
                if (email.equals(cursor.getString(emailIndex))) {
                    return false;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return true;
    }

    /**
     * Проверяет введенные пользователем данные
     * на корректность
     *
     * @param email эл. почта
     * @param name имя пользователя
     * @param password пароль
     * @param passwordRepeat повторно введенный пароль
     *
     * @return true если данные пошли валидацию иначе false
     */
    private boolean isValidData(String email, String name, String password, String passwordRepeat) {
        if (!email.contains("@") || email.contains(" ")) {
            setError(inputEmail, getString(R.string.invalid_email_format));
            return false;
        }
        if (name.isEmpty()) {
            setError(inputName, getString(R.string.enter_name));
            return false;
        }

        if (password.length() < 4) {
            setError(inputPassword, getString(R.string.short_password));
            return false;
        }

        if (!passwordRepeat.equals(password)) {
            setError(inputPasswordRepeat, getString(R.string.passwords_not_equals));
            return false;
        }

        if (password.contains(" ")) {
            setError(inputPassword, getString(R.string.invalid_password_format));
            return false;
        }

        if (password.equalsIgnoreCase(name)) {
            setError(inputPassword, getString(R.string.not_like_name));
            return false;
        }
        return true;
    }

    /**
     * Устанавливает сообщение об ошибке
     * для поля ввода
     *
     * @param editText поле ввода текстовых данных
     * @param msg сообщение
     */
    private void setError(EditText editText, String msg) {
        editText.setError(msg);
    }
}