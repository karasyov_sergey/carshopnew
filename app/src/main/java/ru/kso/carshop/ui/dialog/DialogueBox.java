package ru.kso.carshop.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import ru.kso.carshop.R;
import ru.kso.carshop.activities.MainActivity;
import ru.kso.carshop.helper.IConstants;

/**
 * Класс, представляющий собой диалоговое окно
 *
 * @author KSO 17IT17
 */
public class DialogueBox extends DialogFragment implements
        DialogInterface.OnClickListener, IConstants {
    private Context context;

    public DialogueBox(Context context) {
        super();
        this.context = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        return builder
                .setTitle(getString(R.string.dialog_title))
                .setIcon(R.drawable.ic_log_out)
                .setPositiveButton(getString(R.string.btn_yes), this)
                .setNegativeButton(getString(R.string.btn_no), null)
                .setMessage(getString(R.string.dialog_content))
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        SharedPreferences preferences = context.
                getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =
                preferences.edit();
        editor.clear();
        editor.apply();
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.clearHeader();
            activity.setFragment(R.id.nav_authorization);
        }
    }
}
