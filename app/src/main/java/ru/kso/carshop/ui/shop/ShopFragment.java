package ru.kso.carshop.ui.shop;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ru.kso.carshop.R;
import ru.kso.carshop.adapter.ShopAdapter;
import ru.kso.carshop.db.DBHelper;

/**
 * Класс, представляющий собой фрагмент, отображающий
 * интерфейсные компоненты для автосалона
 *
 * @author KSO 17IT17
 */
public class ShopFragment extends Fragment implements SearchView.OnQueryTextListener {
    private ShopAdapter shopAdapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_shop, container, false);
        initCarList(root);
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Инициализирует интерфейсные компоненты
     * для автосалона
     *
     * @param root базовый интерфейсный блок
     */
    private void initCarList(View root) {
        DBHelper mDBHelper = new DBHelper(root.getContext());
        String query = "select " + DBHelper.KEY_MODEL_ID + ", " +
                DBHelper.KEY_MODEL + ", " + DBHelper.KEY_MODEL_IMG + ", " +
                DBHelper.KEY_PRICE + ", " + DBHelper.KEY_EQUIPMENT + ", " +
                DBHelper.KEY_BRAND + " from " + DBHelper.TABLE_CARS;
        SQLiteDatabase mDatabase = mDBHelper.getWritableDatabase();
        RecyclerView recyclerView = root.findViewById(R.id.shop_list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(layoutManager);
        shopAdapter = new ShopAdapter(root.getContext(), mDatabase, query, null);
        recyclerView.setAdapter(shopAdapter);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem menuItem = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        shopAdapter.getFilter().filter(newText);
        return false;
    }
}