package ru.kso.carshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ru.kso.carshop.R;
import ru.kso.carshop.activities.ModelActivity;
import ru.kso.carshop.db.DBHelper;
import ru.kso.carshop.helper.IConstants;

/**
 * Класс, представляющий собой адаптер для интерфейсного
 * компонента RecyclerView и инициализирующий необходимые
 * данные для отображения автосалона
 *
 * @author KSO 17IT17
 */
public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.CarHolder>
        implements Filterable, IConstants {
    private Context context;
    private SQLiteDatabase db;
    private SparseArray<String> modelImages;
    private SparseArray<String> models;
    private SparseArray<String> brands;
    private SparseIntArray prices;
    private ArrayList<Integer> ids;
    private ArrayList<Integer> idsFull;

    private String[] args;
    private String query;

    public ShopAdapter(Context context, SQLiteDatabase db, String query, String[] args) {
        this.context = context;
        this.db = db;
        this.query = query;
        this.args = args;
        initData();
    }

    /**
     * Инициализирует необходимые данные для
     * их последующего отображения на экране пользователя
     */
    private void initData() {
        modelImages = new SparseArray<>();
        models = new SparseArray<>();
        brands = new SparseArray<>();
        prices = new SparseIntArray();
        ids = new ArrayList<>();

        Cursor cursor = db.rawQuery(query, args);
        if (cursor.moveToFirst()) {
            int modelIndex = cursor.getColumnIndex(DBHelper.KEY_MODEL);
            int imageIndex = cursor.getColumnIndex(DBHelper.KEY_MODEL_IMG);
            int brandIndex = cursor.getColumnIndex(DBHelper.KEY_BRAND);
            int priceIndex = cursor.getColumnIndex(DBHelper.KEY_PRICE);
            int idIndex = cursor.getColumnIndex(DBHelper.KEY_MODEL_ID);
            int id;
            do {
                id = cursor.getInt(idIndex);
                modelImages.put(id, cursor.getString(imageIndex));
                models.put(id, cursor.getString(modelIndex));
                brands.put(id, cursor.getString(brandIndex));
                prices.put(id, cursor.getInt(priceIndex));
                ids.add(id);
            } while (cursor.moveToNext());
        }
        cursor.close();
        idsFull = new ArrayList<>(ids);
    }

    @NonNull
    @Override
    public ShopAdapter.CarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.cell_car, parent, false);
        return new ShopAdapter.CarHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopAdapter.CarHolder holder, int position) {
        position = ids.get(position);
        setImageFromPath(modelImages.get(position), holder.img);
        setTextData(holder.brandName, brands.get(position), R.string.brand);
        setTextData(holder.modelName, models.get(position), R.string.model);
        setTextData(holder.priceData, String.valueOf(prices.get(position)), R.string.price);
        holder.idData = position;
    }

    /**
     * Изменяет содержимое текстового поля
     *
     * @param textView текстовое поле
     * @param string   строка, которую необходимо добавить
     * @param idRes    строковый ресурс, относительно которого форматируется строка
     */
    private void setTextData(TextView textView, String string, int idRes) {
        String temp = context.getString(idRes);
        textView.setText(String.format(temp, string));
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView при помощи библиотеки Glide
     *
     * @param path путь к изображению
     * @param image интерфейсный компонент ImageView
     */
    private void setImageFromPath(String path, ImageView image) {
        Glide.with(context).asBitmap().
                load(path).
                into(image);
    }


    @Override
    public int getItemCount() {
        return ids.size();
    }

    @Override
    public Filter getFilter() {
        return carFilter;
    }

    private Filter carFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ids.clear();
            if (constraint == null || constraint.length() == 0) {
                ids.addAll(idsFull);
            } else {
                int id;
                for (int i = 0; i < brands.size(); i++) {
                    id = idsFull.get(i);
                    if (brands.get(id).toLowerCase().trim().contains(
                            constraint.toString().toLowerCase().trim())) {
                        ids.add(id);
                    }
                }
            }
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    };

    /**
     * Вложенный класс, представляющий собой отдельную
     * ячейку, которая отображает необходимые данные
     * автомобиля
     *
     * @author KSO 17IT17
     */
    public class CarHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Integer idData;
        private TextView priceData;
        private TextView brandName;
        private ImageView img;
        private TextView modelName;

        CarHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.image_model);
            modelName = itemView.findViewById(R.id.model_name);
            brandName = itemView.findViewById(R.id.brand_name);
            priceData = itemView.findViewById(R.id.price_data);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ModelActivity.class);
            intent.putExtra(KEY_ID, idData);
            context.startActivity(intent);
        }
    }
}